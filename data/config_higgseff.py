import ROOT
from xAODAnaHelpers import Config
from ZprimeDM import commonconfig

import itertools

doDetails   =True
histDetail  = "" #2d" #2d debug"
jetDetail   = "kinematic clean layer trackPV energy flavorTag sfFTagFix%s"%(''.join(['%d'%btagWP for btagWP in commonconfig.btagWPs]))
fatjetDetail= "kinematic substructure constituent constituentAll trackJetName_GhostAntiKt2TrackJet bosonCount VTags"

nameSuffix=""
if args.treeName!="outTree":
    isSys=True
    doDetails=False

    histDetail  =""
    jetDetail   ="kinematic clean"
    photonDetail="kinematic"

    nameSuffix="/sys"+args.treeName[7:]
else:
   isSys=False

c = Config()

commonsel={"m_mc"                     : args.is_MC,
           "m_jetDetailStr"           : jetDetail,
           "m_fatjetDetailStr"        : fatjetDetail,
           "m_doCleaning"             : True,
           "m_jetPtCleaningCut"       : 25,
           }

#
# Process Ntuple
#
c.algorithm("MiniTreeEventSelection", { "m_name"                   : "",
                                        "m_mc"                     : args.is_MC,
                                        "m_applyGRL"               : False,
                                        "m_doPUreweighting"        : False,
                                        "m_doTruthOnly"            : False,
                                        "m_triggerDetailStr"       : "passTriggers"
                                        } )

#
# Different selections
config=commonsel.copy()
config.update({"m_name": 'higgseff',
               "m_fatjet0PtCut"   : 450,
               "m_trkjet0_MV2c10" : -2,
               "m_trkjet1_MV2c10" : -2,
               })
c.algorithm("HbbISREffAlgo", config )

config=commonsel.copy()
config.update({"m_name": 'higgseff_tjet77',
               "m_fatjet0PtCut"   : 450,
               "m_trkjet0_MV2c10" : 0.3706,
               "m_trkjet1_MV2c10" : 0.3706,
               })
c.algorithm("HbbISREffAlgo", config )

config=commonsel.copy()
config.update({"m_name": 'higgseff_jet77',
               "m_fatjet0PtCut"   : 450,
               "m_trkjet0_MV2c10" : 0.645925,
               "m_trkjet1_MV2c10" : 0.645925,
               })
c.algorithm("HbbISREffAlgo", config )
