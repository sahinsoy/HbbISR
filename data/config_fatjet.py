import ROOT
from xAODAnaHelpers import Config
from ZprimeDM import commonconfig

import itertools

isSys=False
doDetails=True
histDetailStr   = "" #2d" #2d debug"
jetDetailStr="kinematic clean energy layer trackPV flavorTag %s"%(commonconfig.generate_btag_detailstr())
if args.is_MC: jetDetailStr+=" truth"
fatjetDetailStr= "kinematic substructure constituent constituentAll trackJetName_GhostAntiKt2TrackJet_GhostVR30Rmax4Rmin02TrackJet VTags"
if args.is_MC: fatjetDetailStr+=" bosonCount"
subjetDetailStr="kinematic flavorTag constituent %s"%(commonconfig.generate_btag_detailstr())
muonDetailStr="kinematic"
electronDetailStr="kinematic"

nameSuffix=""
if args.treeName!="outTree":
    isSys=True
    doDetails=False

    histDetailStr  =""
    jetDetailStr   ="kinematic clean"
    photonDetailStr="kinematic"

    nameSuffix="/sys"+args.treeName[7:]
else:
   isSys=False

c = Config()

commonsel={"m_mc"                     : args.is_MC,
           "m_doDetails"              : doDetails,
           "m_histDetailStr"          : histDetailStr,
           "m_jetDetailStr"           : jetDetailStr,
           "m_fatjetDetailStr"        : fatjetDetailStr,
           "m_muonDetailStr"          : muonDetailStr,
           "m_electronDetailStr"      : electronDetailStr,
           "m_doPUReweight"           : True,
           "m_doTrigger"              : False
           }

#
# Process Ntuple
#
c.algorithm("MiniTreeEventSelection", { "m_name"                   : "MiniTreeEventSelection",
                                        "m_mc"                     : args.is_MC,
                                        "m_applyGRL"               : False,
                                        "m_doPUreweighting"        : False,
                                        "m_doTruthOnly"            : False,
                                        "m_triggerDetailStr"       : "passTriggers",
                                        "m_jetDetailStr"           : jetDetailStr,
                                        "m_fatjetDetailStr"        : fatjetDetailStr,
                                        "m_subjetDetailStr"        : subjetDetailStr,
                                        "m_muonDetailStr"          : muonDetailStr,
                                        "m_electronDetailStr"      : electronDetailStr
                                        } )

#
# Different selections
fjpts =[450]
trkjets=['GhostAntiKt2TrackJet','GhostVR30Rmax4Rmin02TrackJet']
ntjets=[0,1,2]
nbtags=[0,1,2]
wps   =['None']+['{btagger}_{btagmode}_{btagWP}'.format(btagger=btagger,btagmode=btagmode,btagWP=btagWP) for btagger,btagmode,btagWP in itertools.product(commonconfig.btaggers,commonconfig.btagmodes,commonconfig.btagWPs)]

for trkjet,fjpt,ntjet,nbtag,wp in itertools.product(trkjets,fjpts,ntjets,nbtags,wps):
    if trkjet=='GhostVR30Rmax4Rmin02TrackJet' and (not wp.startswith('MV2c10_') and wp!='None'): continue # Not present yet
    if nbtag==0 and wp!='None': continue # b-tagging WP doesn't make sense if require no b-tags
    if nbtag>0 and wp=='None': continue # need a b-tagging WP defined if want b-tags
    wp_enum=getattr(ROOT.xAH.Jet,wp) if type(wp)==str else ROOT.xAH.Jet.None

    name=['hbbisr']
    name.append(trkjet)
    name.append('fj%d'%fjpt)
    name.append('nbtag%d'%nbtag)
    if nbtag>0: name.append(wp)
    name.append('tjet%d'%ntjet)
    name='_'.join(name)
    config=commonsel.copy()
    config.update({"m_name"          : name,
                   "m_trkJet"        : trkjet,
                   "m_fatjet0PtCut"  : fjpt,
                   "m_bTagWP"        : wp_enum,
                   "m_nBTagsCut"     : nbtag,
                   "m_nTrackJetsCut" : ntjet
                   })
    if ntjet>=nbtag:
        c.algorithm("HbbISRBtagHistsAlgo", config )

    name=['hbbisr']
    name.append(trkjet)
    name.append('fj%d'%fjpt)
    name.append('tjet%d'%ntjet)
    name.append('nbtag%d'%nbtag)
    if nbtag>0: name.append(wp)
    name='_'.join(name)
    config=commonsel.copy()
    config.update({"m_name"          : name,
                   "m_trkJet"        : trkjet,
                   "m_fatjet0PtCut"  : fjpt,
                   "m_nTrackJetsCut" : ntjet,
                   "m_bTagWP"        : wp_enum,
                   "m_nBTagsCut"     : nbtag
                   })
    if ntjet>=nbtag:
        c.algorithm("HbbISRTJetHistsAlgo", config )
