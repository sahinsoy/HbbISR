import ROOT
from xAODAnaHelpers import Config, utils
if utils.is_release21():
    from ZprimeDM import commonconfig
else:
    from ZprimeDM import commonconfigR20p7 as commonconfig
import datetime

date=datetime.datetime.now().strftime("%Y%m%d")

c = Config()

jetDetailStr="kinematic clean energy layer trackPV flavorTag %s"%(commonconfig.generate_btag_detailstr())
if args.is_MC: jetDetailStr+=" truth"
fatjetDetailStr="kinematic substructure constituent constituentAll trackJetName_GhostAntiKt2TrackJet_GhostVR30Rmax4Rmin02TrackJet VTags"
if args.is_MC: fatjetDetailStr+=" bosonCount"
subjetDetailStr="kinematic flavorTag constituent %s"%(commonconfig.generate_btag_detailstr())
muonDetailStr="kinematic"
elDetailStr="kinematic"

commonconfig.apply_common_config(c,
                                 isMC=args.is_MC,isAFII=args.is_AFII,
                                 triggerSelection='HLT_j15|HLT_j25|HLT_j35|HLT_j60|HLT_j110|HLT_j175|HLT_j260|HLT_j360|HLT_j380|HLT_j420_a10_lcw_L1J100|HLT_j420_a10r_L1J100|HLT_j260_320eta490|HLT_3j200|HLT_4j100|HLT_ht1000_L1J100',
                                 doJets=True,doFatJets=True,doTrackJets=True,doPhotons=False,doMuons=True,doElectrons=True,
                                 btaggers=commonconfig.btaggers,btagmodes=commonconfig.btagmodes,btagWPs=commonconfig.btagWPs)


containers=ROOT.vector('ZprimeNtuplerContainer')()
containers.push_back(ROOT.ZprimeNtuplerContainer(ROOT.ZprimeNtuplerContainer.JET     ,'SignalJets'     ,'jet'   ,jetDetailStr                        ,'kinematic clean'))
containers.push_back(ROOT.ZprimeNtuplerContainer(ROOT.ZprimeNtuplerContainer.FATJET  ,'SignalFatJets'  ,'fatjet',fatjetDetailStr                     ,'kinematic'))
containers.back().m_subjetDetailStr=subjetDetailStr
containers.push_back(ROOT.ZprimeNtuplerContainer(ROOT.ZprimeNtuplerContainer.MUON    ,'SignalMuons'    ,'muon'  ,muonDetailStr                       ,'kinematic'))
containers.push_back(ROOT.ZprimeNtuplerContainer(ROOT.ZprimeNtuplerContainer.ELECTRON,'SignalElectrons','el'    ,elDetailStr                         ,'kinematic'))

c.algorithm("ZprimeNtupler",       { "m_inputAlgo"           : "SignalJets_Algo",
                                     "m_containers"          : containers,
                                     "m_eventDetailStr"      : "pileup",
                                     "m_trigDetailStr"       : "passTriggers"
                                     } )
