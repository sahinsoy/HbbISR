# Installation
To setup the environment for the first time, run the following inside your work directory. To seting up a second time, see the Setup section.
```bash
setupATLAS
lsetup fax panda pyami
voms-proxy-init -voms atlas
git clone git@github.com:UCATLAS/xAODAnaHelpers.git
git clone ssh://git@gitlab.cern.ch:7999/atlas-phys-exotics-dijetisr/ZprimeDM.git
git clone ssh://git@gitlab.cern.ch:7999/atlas-phys-exotics-dijetisr/HbbISR.git
asetup 21.2.4,AnalysisBase,here
mkdir build && cd build
cmake ..
source ${AnalysisBase_PLATFORM}/setup.sh
make
```

# Setup
To setup an already installed environment, run the following inside your work directory.

```bash
setupATLAS
lsetup fax panda pyami
voms-proxy-init -voms atlas
asetup --restore
cd build
source ${AnalysisBase_PLATFORM}/setup.sh
```

To change the location where output is saved (default is `${pwd}`), please define the `DATADIR` environmental variable. For example, on NERSC use
```bash
export DATADIR=/global/projecta/projectdirs/atlas/${USER}/batch
```

# Running
To make ntuples
```bash
./HbbISR/scripts/runntupler.py direct
```

To make plots
```bash
./HbbISR/scripts/runall.sh direct
```