#ifndef HbbISR_HbbISRHists_H
#define HbbISR_HbbISRHists_H

#include <xAODAnaHelpers/HistogramManager.h>

#include <ZprimeDM/EventHists.h>
#include <ZprimeDM/JetHists.h>
#include <ZprimeDM/MuonHists.h>
#include <ZprimeDM/ElectronHists.h>
#include <ZprimeDM/FatJetHists.h>
#include <ZprimeDM/ZprimeResonanceHists.h>

class HbbISRHists : public EventHists
{
public:

  HbbISRHists(const std::string& name, const std::string& detailStr, const std::string& jetDetailStr, const std::string& muonDetailStr, const std::string& electronDetailStr, const std::string& fatjetDetailStr);
  virtual ~HbbISRHists() ;

  virtual void record(EL::Worker *wk);

  StatusCode initialize();
  StatusCode execute(const DijetISREvent& event, const xAH::FatJet *Hcand, float eventWeight);
  StatusCode finalize();
  using HistogramManager::book;    // make other overloaded version of book() to show up in subclass
  using HistogramManager::execute; // make other overloaded version of execute() to show up in subclass

private:
  //
  // histograms
  std::string m_detailStr;

  TH1 *h_fatjetmass_leading;
  TH1 *h_fatjetmass_tau21;
  TH1 *h_fatjetmass_btag;

  ZprimeResonanceHists *m_vbf;

  std::string m_jetDetailStr;
  TH1F*     h_nJet;
  ZprimeDM::JetHists *m_jet0;
  ZprimeDM::JetHists *m_jet1;
  ZprimeDM::JetHists *m_jet2;
  ZprimeDM::JetHists *m_jet3;

  std::string m_muonDetailStr;
  TH1F*     h_nMuon;
  ZprimeDM::MuonHists *m_muon0;
  ZprimeDM::MuonHists *m_muon1;
  ZprimeDM::MuonHists *m_muon2;

  std::string m_electronDetailStr;
  TH1F*     h_nElectron;
  ZprimeDM::ElectronHists *m_electron0;
  ZprimeDM::ElectronHists *m_electron1;
  ZprimeDM::ElectronHists *m_electron2;

  std::string m_fatjetDetailStr;
  TH1F*     h_nFatJet;
  ZprimeDM::FatJetHists *m_Hcand;
  ZprimeDM::FatJetHists *m_fatjet0;
};

#endif // HbbISR_HbbISRHists_H
