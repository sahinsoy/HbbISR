#ifndef HbbISR_HbbISRTJetHistsAlgo_H
#define HbbISR_HbbISRTJetHistsAlgo_H

// algorithm wrapper
#include <HbbISR/HbbISRHistsBaseAlgo.h>

class HbbISRTJetHistsAlgo : public HbbISRHistsBaseAlgo
{
public:
  std::string m_trkJet; // Name of the track jet collection

  uint m_nTrackJetsCut; // Minimum number of track jets to define the Higgs candidate
  xAH::Jet::BTaggerOP m_bTagWP; // Btagging working point.
  float m_bTagCut; // Btagging MV2c10 cut, set m_bTagWP=None to use
  uint m_nBTagsCut; // Minimum number of b-tagged track jets

protected:
  virtual void initISRCutflow();
  virtual bool doISRCutflow();

public:
  // this is a standard constructor
  HbbISRTJetHistsAlgo ();

private:
  //
  // cutflow
  int m_cf_nfatjets;
  int m_cf_ntjet;
  int m_cf_nbtag;

  // this is needed to distribute the algorithm to the workers
  ClassDef(HbbISRTJetHistsAlgo, 1);
};

#endif // HbbISR_HbbISRTJetHistsAlgo_H
