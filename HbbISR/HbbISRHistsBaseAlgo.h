#ifndef HbbISR_HbbISRHistsBaseAlgo_H
#define HbbISR_HbbISRHistsBaseAlgo_H

#include <EventLoop/StatusCode.h>
#include <EventLoop/Algorithm.h>
#include <EventLoop/Worker.h>

//algorithm wrapper
#include <xAODAnaHelpers/Algorithm.h>

// our histogramming code
#include <ZprimeDM/DijetISREvent.h>
#include <ZprimeDM/CutflowHists.h>

#include <HbbISR/HbbISRHists.h>

#include <sstream>
#include <vector>

class HbbISRHistsBaseAlgo : public xAH::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  
  //configuration variables
  bool m_debug;
  bool m_mc;

  // switches
  std::string m_histDetailStr;
  std::string m_jetDetailStr;
  std::string m_fatjetDetailStr;
  std::string m_muonDetailStr;
  std::string m_electronDetailStr;

  bool m_doDetails;          // Make histograms for exlusive selections selections
  bool m_doPUReweight;

  // trigger config
  bool m_doTrigger;  
  /** @brief Comma-separated list of triggers that must pass (AND) */
  std::string m_trigger;

  // Kinematic selection
  float m_fatjet0PtCut;

private:

  //
  // Cutflow
  int m_cf_trigger;
  int m_cf_fatjet0;
  // ISR cuts


  //
  // Histograms
  HbbISRHists *hIncl; //!

  HbbISRHists *hM50to100;  //!
  HbbISRHists *hM75to125;  //!
  HbbISRHists *hM100to150; //!
  HbbISRHists *hM125to175; //!
  HbbISRHists *hM150to200; //!
  HbbISRHists *hM175to225; //!
  HbbISRHists *hM200to250; //!
  HbbISRHists *hM225to275; //!
  HbbISRHists *hM250to300; //!

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)

protected:
  // Cutflow data
  CutflowHists *m_cutflow; //!

  // Event data
  DijetISREvent *m_event; //!

  //
  // Parsed configuration
  std::vector<std::string> m_triggers;

  // ISR selection
  float m_eventWeight;
  const xAH::FatJet *m_Hcand; //!

  //
  // functions
  virtual void initISRCutflow() =0;
  virtual bool doISRCutflow() =0;

public:
  // Tree *myTree; //!
  // TH1 *myHist; //!

  // this is a standard constructor
  HbbISRHistsBaseAlgo ();

  // these are the functions inherited from Algorithm
  EL::StatusCode histInitialize ();
  EL::StatusCode initialize ();
  EL::StatusCode execute ();
  EL::StatusCode histFill (float eventWeight);
  EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(HbbISRHistsBaseAlgo, 1);
};

#endif // HbbISR_HbbISRHistsBaseAlgo_H
