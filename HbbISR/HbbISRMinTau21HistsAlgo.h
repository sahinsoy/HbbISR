#ifndef HbbISR_HbbISRMinTau21HistsAlgo_H
#define HbbISR_HbbISRMinTau21HistsAlgo_H

// algorithm wrapper
#include <HbbISR/HbbISRHistsBaseAlgo.h>

class HbbISRMinTau21HistsAlgo : public HbbISRHistsBaseAlgo
{
public:

protected:
  virtual void initISRCutflow();
  virtual bool doISRCutflow();

public:
  // this is a standard constructor
  HbbISRMinTau21HistsAlgo ();

private:
  //
  // cutflow
  int m_cf_nfatjets;

  // this is needed to distribute the algorithm to the workers
  ClassDef(HbbISRMinTau21HistsAlgo, 1);
};

#endif // HbbISR_HbbISRMinTau21HistsAlgo_H
