#include <HbbISR/HbbISRTJetHistsAlgo.h>

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

#include <AsgTools/MessageCheck.h>

#include <xAODAnaHelpers/HelperFunctions.h>
#include <xAODAnaHelpers/HelperClasses.h>

#include <algorithm>

// this is needed to distribute the algorithm to the workers
ClassImp(HbbISRTJetHistsAlgo)

HbbISRTJetHistsAlgo :: HbbISRTJetHistsAlgo ()
: HbbISRHistsBaseAlgo(),
  m_trkJet("GhostAntiKt2TrackJet"),
  m_nTrackJetsCut(0),
  m_bTagWP(xAH::Jet::BTaggerOP::None),
  m_bTagCut(0),
  m_nBTagsCut(0)
{ }

void HbbISRTJetHistsAlgo :: initISRCutflow ()
{
  m_cf_nfatjets = m_cutflow->addCut("NFatJets");
  if(m_nTrackJetsCut>0) m_cf_ntjet = m_cutflow->addCut("NTrackJet");
  if(m_nBTagsCut>0)     m_cf_nbtag = m_cutflow->addCut("NBTag");
}

bool HbbISRTJetHistsAlgo :: doISRCutflow ()
{
  uint nfatjets = m_event->fatjets();

  if(nfatjets < 1)
    {
      ANA_MSG_DEBUG(" Fail NFatJets with " << nfatjets);
      return false;
    }
  m_cutflow->execute(m_cf_nfatjets,m_eventWeight);

  //
  // Choose h-cand
  m_Hcand=0;
  for(uint i=0;i<m_event->fatjets();i++)
    {
      const xAH::FatJet *fatjet=m_event->fatjet(i);
      if(fatjet->trkJets.size()>=m_nTrackJetsCut)
	{
	  m_Hcand=fatjet;
	  break;
	}
    }
  if(m_nTrackJetsCut>0)
    {
      if(m_Hcand==0)
	{
	  ANA_MSG_DEBUG(" Fail NTrackJet");
	  return false;
	}
      m_cutflow->execute(m_cf_ntjet,m_eventWeight);
    }

  //
  // Find btagged jet
  if(m_nBTagsCut>0)
    {
      unsigned int nbtags=0;
      for(const auto& trkJet : m_Hcand->trkJets.at(m_trkJet))
	if((m_bTagWP!=xAH::Jet::None)?trkJet.is_btag(m_bTagWP):trkJet.MV2c10>m_bTagCut) nbtags++;
      if(nbtags<m_nBTagsCut)
	{
	  ANA_MSG_DEBUG(" Fail NBTag");
	  return false;	  
	}
      m_cutflow->execute(m_cf_nbtag,m_eventWeight);
    }

  return true;
}

