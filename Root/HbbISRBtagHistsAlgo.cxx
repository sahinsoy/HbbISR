#include <HbbISR/HbbISRBtagHistsAlgo.h>

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

#include <AsgTools/MessageCheck.h>

#include <xAODAnaHelpers/HelperFunctions.h>
#include <xAODAnaHelpers/HelperClasses.h>

#include <algorithm>

// this is needed to distribute the algorithm to the workers
ClassImp(HbbISRBtagHistsAlgo)

HbbISRBtagHistsAlgo :: HbbISRBtagHistsAlgo ()
: HbbISRHistsBaseAlgo(),
  m_trkJet("GhostAntiKt2TrackJet"),
  m_bTagWP(xAH::Jet::BTaggerOP::None),
  m_bTagCut(0),
  m_nBTagsCut(0),
  m_nTrackJetsCut(0)
{ }

void HbbISRBtagHistsAlgo :: initISRCutflow ()
{
  m_cf_nfatjets = m_cutflow->addCut("NFatJets");
  if(m_nBTagsCut>0)     m_cf_nbtag = m_cutflow->addCut("NBTag");
  if(m_nTrackJetsCut>0) m_cf_ntjet = m_cutflow->addCut("NTrackJet");
}

bool HbbISRBtagHistsAlgo :: doISRCutflow ()
{
  uint nfatjets = m_event->fatjets();

  if(nfatjets < 1)
    {
      ANA_MSG_DEBUG(" Fail NFatJets with " << nfatjets);
      return false;
    }
  m_cutflow->execute(m_cf_nfatjets,m_eventWeight);

  //
  // Choose h-cand
  m_Hcand=0;
  for(uint i=0;i<m_event->fatjets();i++)
    {
      const xAH::FatJet *fatjet=m_event->fatjet(i);
      // Find btagged jet
      unsigned int nbtags=0;
      for(const auto& trkJet : fatjet->trkJets.at(m_trkJet))
	if((m_bTagWP!=xAH::Jet::None)?trkJet.is_btag(m_bTagWP):trkJet.MV2c10>m_bTagCut) nbtags++;
      if(nbtags>=m_nBTagsCut)
	{
	  m_Hcand=fatjet;
	  break;
	}
    }
  if(m_nBTagsCut>0)
    {
      if(m_Hcand==0)
	{
	  ANA_MSG_DEBUG(" Fail NBTag");
	  return false;
	}
      m_cutflow->execute(m_cf_nbtag,m_eventWeight);
    }

  //
  // Number of track jets
  if(m_nTrackJetsCut>0)
    {
      if(m_Hcand->trkJets.size()<m_nTrackJetsCut)
	{
	  ANA_MSG_DEBUG(" Fail NTrackJet with " << m_Hcand->trkJets.size());
	  return false;	  
	}
      m_cutflow->execute(m_cf_ntjet,m_eventWeight);
    }

  return true;
}

