/* Plotting Tools */
#include <HbbISR/HbbISRHistsBaseAlgo.h>
#include <HbbISR/HbbISRBtagHistsAlgo.h>
#include <HbbISR/HbbISRTJetHistsAlgo.h>
#include <HbbISR/HbbISRMinTau21HistsAlgo.h>
#include <HbbISR/HbbISREffAlgo.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ class HbbISRHistsBaseAlgo+;
#pragma link C++ class HbbISRBtagHistsAlgo+;
#pragma link C++ class HbbISRTJetHistsAlgo+;
#pragma link C++ class HbbISRMinTau21HistsAlgo+;
#pragma link C++ class HbbISREffAlgo+;

#endif
