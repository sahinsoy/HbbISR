#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include <EventLoop/OutputStream.h>

#include <AsgTools/MessageCheck.h>

#include <HbbISR/HbbISRHistsBaseAlgo.h>
#include <xAODAnaHelpers/HelperFunctions.h>

#include "TFile.h"
#include "TKey.h"
#include "TLorentzVector.h"
#include "TSystem.h"

#include <utility>      
#include <iostream>
#include <fstream>

using namespace std;

// this is needed to distribute the algorithm to the workers
ClassImp(HbbISRHistsBaseAlgo)

HbbISRHistsBaseAlgo :: HbbISRHistsBaseAlgo () :
  m_mc(false),
  m_histDetailStr(""),
  m_jetDetailStr(""),
  m_fatjetDetailStr(""),
  m_muonDetailStr(""),
  m_electronDetailStr(""),
  m_doDetails(false),
  m_doPUReweight(false),
  m_doTrigger(false),
  m_trigger(""),
  m_fatjet0PtCut(0.),
  hIncl(nullptr),
  hM50to100 (nullptr),
  hM75to125 (nullptr),
  hM100to150(nullptr),
  hM125to175(nullptr),
  hM150to200(nullptr),
  hM175to225(nullptr),
  hM200to250(nullptr),
  hM225to275(nullptr),
  hM250to300(nullptr)
{
  Info("HbbISRHistsBaseAlgo()", "Calling constructor");
}

EL::StatusCode HbbISRHistsBaseAlgo :: histInitialize ()
{
  Info("histInitialize()", "Calling histInitialize \n");

  //
  // data model
  m_event=DijetISREvent::global();

  //
  // Cutflow
  m_cutflow=new CutflowHists(m_name, "");
  ANA_CHECK(m_cutflow->initialize());

  m_cf_trigger  =m_cutflow->addCut("trigger");
  if(m_fatjet0PtCut>0) m_cf_fatjet0  =m_cutflow->addCut("fatjet0");
  initISRCutflow();

  m_cutflow->record(wk());

  //
  // Histograms
  hIncl      =new HbbISRHists(m_name, m_histDetailStr, m_jetDetailStr, m_muonDetailStr, m_electronDetailStr, m_fatjetDetailStr);
  ANA_CHECK(hIncl->initialize());
  hIncl->record(wk());

  if(!m_doDetails) return EL::StatusCode::SUCCESS;

  hM50to100 =new HbbISRHists(m_name+"/M50to100/" , "", "kinematic", "kinematic", "kinematic", m_fatjetDetailStr);
  ANA_CHECK(hM50to100 ->initialize());
  hM50to100->record(wk());

  hM75to125 =new HbbISRHists(m_name+"/M75to125/" , "", "kinematic", "kinematic", "kinematic", m_fatjetDetailStr);
  ANA_CHECK(hM75to125 ->initialize());
  hM75to125->record(wk());

  hM100to150=new HbbISRHists(m_name+"/M100to150/", "", "kinematic", "kinematic", "kinematic", m_fatjetDetailStr);
  ANA_CHECK(hM100to150->initialize());
  hM100to150->record(wk());

  hM125to175=new HbbISRHists(m_name+"/M125to175/", "", "kinematic", "kinematic", "kinematic", m_fatjetDetailStr);
  ANA_CHECK(hM125to175->initialize());
  hM125to175->record(wk());

  hM150to200=new HbbISRHists(m_name+"/M150to200/", "", "kinematic", "kinematic", "kinematic", m_fatjetDetailStr);
  ANA_CHECK(hM150to200->initialize());
  hM150to200->record(wk());

  hM175to225=new HbbISRHists(m_name+"/M175to225/" , "", "kinematic", "kinematic", "kinematic", m_fatjetDetailStr);
  ANA_CHECK(hM175to225 ->initialize());
  hM175to225->record(wk());

  hM200to250=new HbbISRHists(m_name+"/M200to250/", "", "kinematic", "kinematic", "kinematic", m_fatjetDetailStr);
  ANA_CHECK(hM200to250->initialize());
  hM200to250->record(wk());

  hM225to275=new HbbISRHists(m_name+"/M225to275/", "", "kinematic", "kinematic", "kinematic", m_fatjetDetailStr);
  ANA_CHECK(hM225to275->initialize());
  hM225to275->record(wk());

  hM250to300=new HbbISRHists(m_name+"/M250to300/", "", "kinematic", "kinematic", "kinematic", m_fatjetDetailStr);
  ANA_CHECK(hM250to300->initialize());
  hM250to300->record(wk());

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode HbbISRHistsBaseAlgo :: initialize ()
{
  if(m_debug) Info("initialize()", "Calling initialize");

  // Trigger
  std::string token;
  std::istringstream ss(m_trigger);

  while(std::getline(ss, token, ','))
    m_triggers.push_back(token);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode HbbISRHistsBaseAlgo :: execute ()
{
  ANA_MSG_DEBUG("HbbISRHistsBaseAlgo::execute()");

  //
  // Cuts
  m_eventWeight    =m_event->m_weight;
  if(m_mc && m_doPUReweight)
    m_eventWeight *= m_event->m_weight_pileup;

  //
  // do trigger
  if(m_doTrigger)
    {
      //
      // trigger
      bool passTrigger=true;
      for(const std::string& trigger : m_triggers)
	passTrigger &= (std::find(m_event->m_passedTriggers->begin(), m_event->m_passedTriggers->end(), trigger ) != m_event->m_passedTriggers->end());

      if(!passTrigger)
	{
	  ANA_MSG_DEBUG(" Fail Trigger");
	  return EL::StatusCode::SUCCESS;
	}
      m_cutflow->execute(m_cf_trigger, m_eventWeight);
    }

  //
  // Kinematic cuts
  if(m_fatjet0PtCut>0)
    {
      if(m_event->fatjets()<1)
	{
	  ANA_MSG_DEBUG(" Fail fatjetPt0 with 0 jets");
	  return EL::StatusCode::SUCCESS;
	}

      const xAH::FatJet* fatjet0=m_event->fatjet(0);
      if(fatjet0->p4.Pt() < m_fatjet0PtCut)
	{
	  ANA_MSG_DEBUG(" Fail fatjetPt0 with " << fatjet0->p4.Pt());
	  return EL::StatusCode::SUCCESS;
	}
      m_cutflow->execute(m_cf_fatjet0,m_eventWeight);
    }

  //
  // ISR part of the selection
  if(!doISRCutflow())
    return EL::StatusCode::SUCCESS;

  ANA_MSG_DEBUG(" Pass All Cut");

  ANA_CHECK(histFill(m_eventWeight));

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode HbbISRHistsBaseAlgo::histFill(float eventWeight)
{
  //
  //Filling
  hIncl->execute(*m_event, m_Hcand, eventWeight);

  if(!m_doDetails) return EL::StatusCode::SUCCESS;

  double m  =m_Hcand->p4.M();

  if(m >  50 && m < 100)
    hM50to100 ->execute(*m_event, m_Hcand, eventWeight);

  if(m >  75 && m < 125)
    hM75to125 ->execute(*m_event, m_Hcand, eventWeight);

  if(m > 100 && m < 150)
    hM100to150->execute(*m_event, m_Hcand, eventWeight);

  if(m > 125 && m < 175)
    hM125to175->execute(*m_event, m_Hcand, eventWeight);

  if(m > 150 && m < 200)
    hM150to200->execute(*m_event, m_Hcand, eventWeight);

  if(m > 175 && m < 225)
    hM175to225->execute(*m_event, m_Hcand, eventWeight);

  if(m > 200 && m < 250)
    hM200to250->execute(*m_event, m_Hcand, eventWeight);

  if(m > 225 && m < 275)
    hM225to275->execute(*m_event, m_Hcand, eventWeight);

  if(m > 250 && m < 300)
    hM250to300->execute(*m_event, m_Hcand, eventWeight);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode HbbISRHistsBaseAlgo :: histFinalize ()
{
  ANA_CHECK(m_cutflow->finalize());
  delete m_cutflow;

  ANA_CHECK(hIncl->finalize());
  delete hIncl;

  if(!m_doDetails) return EL::StatusCode::SUCCESS;

  ANA_CHECK(hM50to100->finalize());
  delete hM50to100;

  ANA_CHECK(hM75to125->finalize());
  delete hM75to125;

  ANA_CHECK(hM100to150->finalize());
  delete hM100to150;

  ANA_CHECK(hM125to175->finalize());
  delete hM125to175;

  ANA_CHECK(hM150to200->finalize());
  delete hM150to200;

  ANA_CHECK(hM175to225->finalize());
  delete hM175to225;

  ANA_CHECK(hM200to250->finalize());
  delete hM200to250;

  ANA_CHECK(hM225to275->finalize());
  delete hM225to275;

  ANA_CHECK(hM250to300->finalize());
  delete hM250to300;

  return EL::StatusCode::SUCCESS;
}

