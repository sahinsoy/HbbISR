#include <HbbISR/HbbISRMinTau21HistsAlgo.h>

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

#include <AsgTools/MessageCheck.h>

#include <xAODAnaHelpers/HelperFunctions.h>
#include <xAODAnaHelpers/HelperClasses.h>

#include <algorithm>

// this is needed to distribute the algorithm to the workers
ClassImp(HbbISRMinTau21HistsAlgo)

HbbISRMinTau21HistsAlgo :: HbbISRMinTau21HistsAlgo ()
: HbbISRHistsBaseAlgo()
{ }

void HbbISRMinTau21HistsAlgo :: initISRCutflow ()
{
  m_cf_nfatjets    = m_cutflow->addCut("NFatJets");
}

bool HbbISRMinTau21HistsAlgo :: doISRCutflow ()
{
  uint nfatjets = m_event->fatjets();

  if(nfatjets < 1)
    {
      if(m_debug) std::cout << " Fail NFatJets with " << nfatjets << std::endl;
      return false;
    }
  m_cutflow->execute(m_cf_nfatjets,m_eventWeight);

  //
  // Setup objects
  //
  m_Hcand=0;
  for(uint i=0;i<m_event->fatjets();i++)
    {
      const xAH::FatJet *fatjet=m_event->fatjet(i);
      // Best tau21
      if(m_Hcand==0 || m_Hcand->tau21_wta>fatjet->tau21_wta)
	m_Hcand=fatjet;
    }

  return true;
}

