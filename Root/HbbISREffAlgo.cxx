#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include <EventLoop/OutputStream.h>

#include <AsgTools/MessageCheck.h>

#include <HbbISR/HbbISREffAlgo.h>
#include <xAODAnaHelpers/HelperFunctions.h>

#include <utility>      
#include <iostream>
#include <fstream>

using namespace std;

// this is needed to distribute the algorithm to the workers
ClassImp(HbbISREffAlgo)

HbbISREffAlgo :: HbbISREffAlgo () :
  m_mc(false),
  m_jetDetailStr(""),
  m_fatjetDetailStr(""),
  m_doCleaning(false),
  m_jetPtCleaningCut(25.),
  m_fatjet0PtCut(0),
  m_trkjet0_MV2c10(-2),
  m_trkjet1_MV2c10(-2),
  h_hjet(nullptr),
  h_bjet0(nullptr),
  h_bjet1(nullptr),
  h_hjet_idx(nullptr),
  h_hjet_2trkidx(nullptr),
  h_bjet_idx(nullptr)
{
  ANA_MSG_INFO("HbbISREffAlgo::HbbISREffAlgo()");
}

EL::StatusCode HbbISREffAlgo :: histInitialize ()
{
  ANA_MSG_INFO("HbbISREffAlgo::histInitialize()");

  //
  // data model
  m_event=DijetISREvent::global();

  //
  // Cutflow
  m_cutflow=new CutflowHists(m_name, "");
  ANA_CHECK(m_cutflow->initialize());

  if(m_doCleaning)     m_cf_cleaning=m_cutflow->addCut("cleaning");
  if(m_fatjet0PtCut>0) m_cf_fatjet0 =m_cutflow->addCut("fatjet0");

  m_cutflow->record(wk());

  //
  // Histograms
  h_hjet =new ZprimeDM::FatJetHists(m_name+"/hjet", m_fatjetDetailStr, "higgs");
  ANA_CHECK(h_hjet->initialize());
  h_hjet->record(wk());

  h_bjet0 =new ZprimeDM::JetHists(m_name+"/bjet0", m_jetDetailStr, "leading b-jet");
  ANA_CHECK(h_bjet0->initialize());
  h_bjet0->record(wk());

  h_bjet1 =new ZprimeDM::JetHists(m_name+"/bjet1", m_jetDetailStr, "subleading b-jet");
  ANA_CHECK(h_bjet1->initialize());
  h_bjet1->record(wk());

  h_hjet_idx= new TH1F((m_name+"/hjet_idx").c_str(), "", 6, -1.5, 4.5);
  h_hjet_idx->GetXaxis()->SetTitle("higgs jet index");
  wk()->addOutput(h_hjet_idx);
  h_hjet_2trkidx= new TH1F((m_name+"/hjet_2trkidx").c_str(), "", 6, -1.5, 4.5);
  h_hjet_2trkidx->GetXaxis()->SetTitle("higgs 2trkjet index");
  wk()->addOutput(h_hjet_2trkidx);
  h_bjet_idx= new TH2F((m_name+"/bjet_idx").c_str(), "", 6, -1.5, 4.5, 6, -1.5, 4.5);
  h_bjet_idx->GetXaxis()->SetTitle("leading b-jet index");
  h_bjet_idx->GetYaxis()->SetTitle("subleading b-jet index");
  wk()->addOutput(h_bjet_idx);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode HbbISREffAlgo :: execute ()
{
  ANA_MSG_DEBUG("HbbISREffAlgo::execute()");

  //
  // Cuts
  float eventWeight    =m_event->m_weight;

  //
  //  Jet Cleaning 
  if(m_doCleaning)
    {
      bool  passCleaning   = true;
      for(unsigned int i = 0;  i<m_event->jets(); ++i)
	{
	  const xAH::Jet* jet=m_event->jet(i);
	  if(jet->p4.Pt() > m_jetPtCleaningCut)
	    {
	      if(!m_doCleaning && !jet->clean_passLooseBad)
		{
		  ANA_MSG_DEBUG(" Skipping jet clean");
		  continue;
		}
	      if(!jet->clean_passLooseBad) passCleaning = false;
	    }
	  else
	    break;
	}

      if(!passCleaning)
	{
	  ANA_MSG_DEBUG(" Fail cleaning");
	  return EL::StatusCode::SUCCESS;
	}
      m_cutflow->execute(m_cf_cleaning, eventWeight);
    }

  //
  // Leading jet pt
  if(m_fatjet0PtCut>0)
    {
      if(m_event->fatjets()==0)
	{
	  ANA_MSG_DEBUG(" Fail fatjet count");
	  return EL::StatusCode::SUCCESS;
	}
      if(m_event->fatjet(0)->p4.Pt()<m_fatjet0PtCut)
	{
	  ANA_MSG_DEBUG(" Fail fatjet pt with " << m_event->fatjet(0)->p4.Pt());
	  return EL::StatusCode::SUCCESS;
	}
      m_cutflow->execute(m_cf_fatjet0, eventWeight);
    }

  ANA_MSG_DEBUG(" Pass All Cut");

  //
  // Higgs stuff
  //

  // Identify truth higgs
  const xAH::FatJet *hjet=0;
  uint itrkjet=0;
  for(uint i=0;i<m_event->fatjets();i++)
    {
      const xAH::FatJet *fatjet=m_event->fatjet(i);
      if(fatjet->nHBosons>0)
	{
	  h_hjet_idx->Fill(i, eventWeight);
	  if(fatjet->trkJets.size()>=2) h_hjet_2trkidx->Fill(itrkjet, eventWeight);
	  else h_hjet_2trkidx->Fill(-1, eventWeight);
	  hjet=fatjet;
	  break;
	}
      if(fatjet->trkJets.size()>=2) itrkjet++;
    }
  if(hjet==0)
    {
      h_hjet_2trkidx ->Fill(-1, eventWeight);
      h_hjet_idx     ->Fill(-1, eventWeight);
      return EL::StatusCode::SUCCESS;
    }

  // Identify b-track jets
  const xAH::Jet *bjet0=0;
  const xAH::Jet *bjet1=0;
  uint i=0;
  int bjet0_idx=-1;
  int bjet1_idx=-1;
  for(const xAH::Jet& trkjet : hjet->trkJets.at(m_trkJet))
    {
      if(trkjet.HadronConeExclTruthLabelID==5)
	{
	  if(bjet0==0)
	    {
	      bjet0_idx=i;
	      bjet0=&trkjet;
	    }
	  else if(bjet1==0)
	    {
	      bjet1_idx=i;
	      bjet1=&trkjet;
	      break;
	    }
	}
      i++;
    }

  h_bjet_idx->Fill(bjet0_idx,bjet1_idx, eventWeight);
  if(bjet0!=0)
    if(bjet0->MV2c10>m_trkjet0_MV2c10) h_bjet0->execute(bjet0, eventWeight);

  if(bjet1!=0)
    if(bjet1->MV2c10>m_trkjet1_MV2c10) h_bjet1->execute(bjet1, eventWeight);

  if((bjet0!=0 && bjet0->MV2c10>m_trkjet0_MV2c10) &&
     (bjet1!=0 && bjet1->MV2c10>m_trkjet1_MV2c10))
    h_hjet->execute(hjet, eventWeight);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode HbbISREffAlgo :: histFinalize ()
{
  ANA_CHECK(m_cutflow->finalize());
  delete m_cutflow;

  ANA_CHECK(h_hjet->finalize());
  delete h_hjet;

  ANA_CHECK(h_bjet0->finalize());
  delete h_bjet0;

  ANA_CHECK(h_bjet1->finalize());
  delete h_bjet1;

  return EL::StatusCode::SUCCESS;
}

