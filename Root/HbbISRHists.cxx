#include <HbbISR/HbbISRHists.h>

#include <sstream>

HbbISRHists :: HbbISRHists (const std::string& name, const std::string& detailStr, const std::string& jetDetailStr, const std::string& muonDetailStr, const std::string& electronDetailStr, const std::string& fatjetDetailStr)
  : EventHists(name, detailStr),
    m_detailStr(detailStr), m_vbf(0),
    m_jetDetailStr(jetDetailStr), h_nJet(0), m_jet0(0), m_jet1(0), m_jet2(0), m_jet3(0),
    m_muonDetailStr(muonDetailStr), h_nMuon(0), m_muon0(0), m_muon1(0), m_muon2(0),
    m_electronDetailStr(electronDetailStr), h_nElectron(0), m_electron0(0), m_electron1(0), m_electron2(0),
    m_fatjetDetailStr(fatjetDetailStr), h_nFatJet(0), m_fatjet0(0)
{ }

HbbISRHists :: ~HbbISRHists () 
{ }

void HbbISRHists::record(EL::Worker* wk)
{
  EventHists::record(wk);

  m_vbf         ->record(wk);

  if(!m_jetDetailStr.empty())
    {
      m_jet0     ->record(wk);
      m_jet1     ->record(wk);
      m_jet2     ->record(wk);
      m_jet3     ->record(wk);
    }

  if(!m_muonDetailStr.empty())
    {
      m_muon0    ->record(wk);
      m_muon1    ->record(wk);
      m_muon2    ->record(wk);
    }

  if(!m_electronDetailStr.empty())
    {
      m_electron0->record(wk);
      m_electron1->record(wk);
      m_electron2->record(wk);
    }

  if(!m_fatjetDetailStr.empty())
    {
      m_Hcand    ->record(wk);
      m_fatjet0  ->record(wk);
    }
}

StatusCode HbbISRHists::initialize()
{
  if(m_debug) std::cout << "HbbISRHists::initialize()" << std::endl;

  ANA_CHECK(EventHists::initialize());

  m_vbf          =new ZprimeResonanceHists(m_name+"vbf_", m_detailStr);
  ANA_CHECK(m_vbf->initialize());

  if(!m_jetDetailStr.empty())
    {
      h_nJet     = book(m_name, "nJets", "N_{jets}", 10, -0.5, 9.5 );

      m_jet0=new ZprimeDM::JetHists(m_name+"jet0_" , m_jetDetailStr, "leading");
      ANA_CHECK(m_jet0->initialize());

      m_jet1=new ZprimeDM::JetHists(m_name+"jet1_" , m_jetDetailStr, "subleading");
      ANA_CHECK(m_jet1->initialize());

      m_jet2=new ZprimeDM::JetHists(m_name+"jet2_" , m_jetDetailStr, "third");
      ANA_CHECK(m_jet2->initialize());

      m_jet3=new ZprimeDM::JetHists(m_name+"jet3_" , m_jetDetailStr, "fourth");
      ANA_CHECK(m_jet3->initialize());
    }

  if(!m_muonDetailStr.empty())
    {
      h_nMuon     = book(m_name, "nMuons", "N_{#mu}", 10, -0.5, 9.5 );

      m_muon0=new ZprimeDM::MuonHists(m_name+"muon0_" , m_muonDetailStr, "leading");
      ANA_CHECK(m_muon0->initialize());

      m_muon1=new ZprimeDM::MuonHists(m_name+"muon1_" , m_muonDetailStr, "subleading");
      ANA_CHECK(m_muon1->initialize());

      m_muon2=new ZprimeDM::MuonHists(m_name+"muon2_" , m_muonDetailStr, "third");
      ANA_CHECK(m_muon2->initialize());
    }

  if(!m_electronDetailStr.empty())
    {
      h_nElectron     = book(m_name, "nElectrons", "N_{el}", 10, -0.5, 9.5 );

      m_electron0=new ZprimeDM::ElectronHists(m_name+"el0_" , m_electronDetailStr, "leading");
      ANA_CHECK(m_electron0->initialize());

      m_electron1=new ZprimeDM::ElectronHists(m_name+"el1_" , m_electronDetailStr, "subleading");
      ANA_CHECK(m_electron1->initialize());

      m_electron2=new ZprimeDM::ElectronHists(m_name+"el2_" , m_electronDetailStr, "third");
      ANA_CHECK(m_electron2->initialize());
    }

  if(!m_fatjetDetailStr.empty())
    {
      h_nFatJet  = book(m_name, "nFatJets",  "N_{fatjet}",    10,     -0.5,     9.5 );

      m_Hcand   =new ZprimeDM::FatJetHists(m_name+"Hcand_", m_fatjetDetailStr, "H candidate");
      ANA_CHECK(m_Hcand  ->initialize());

      m_fatjet0 =new ZprimeDM::FatJetHists(m_name+"fatjet0_", m_fatjetDetailStr, "leading");
      ANA_CHECK(m_fatjet0->initialize());
    }

  return StatusCode::SUCCESS;
}

StatusCode HbbISRHists::execute(const DijetISREvent& event, const xAH::FatJet *Hcand, float eventWeight)
{
  if(m_debug) std::cout << "HbbISRHists::execute()" << std::endl;
  ANA_CHECK(EventHists::execute(event, eventWeight));

  //
  const xAH::Jet *vbf0=0;
  const xAH::Jet *vbf1=0;
  for(uint i=0; i<event.jets(); i++)
    {
      const xAH::Jet *vbfcand=event.jet(i);
      if(Hcand->p4.DeltaR(vbfcand->p4)>1.)
	{
	  if(vbf0==0) vbf0=vbfcand;
	  else vbf1=vbfcand;
	  if(vbf1!=0) break;
	}
    }
  if(vbf0 && vbf1)
    ANA_CHECK(m_vbf->execute(vbf0, vbf1, eventWeight));

  //
  // Jets
  if(!m_jetDetailStr.empty())
    {
      h_nJet   ->Fill(event.jets   (), eventWeight);

      if(event.jets     ()>0) ANA_CHECK(m_jet0     ->execute(event.jet(0)   , eventWeight));
      if(event.jets     ()>1) ANA_CHECK(m_jet1     ->execute(event.jet(1)   , eventWeight));
      if(event.jets     ()>2) ANA_CHECK(m_jet2     ->execute(event.jet(2)   , eventWeight));
      if(event.jets     ()>3) ANA_CHECK(m_jet3     ->execute(event.jet(3)   , eventWeight));
    }

  //
  // Muons
  if(!m_muonDetailStr.empty())
    {
      h_nMuon   ->Fill(event.muons   (), eventWeight);

      if(event.muons    ()>0) ANA_CHECK(m_muon0    ->execute(event.muon(0)   , eventWeight));
      if(event.muons    ()>1) ANA_CHECK(m_muon1    ->execute(event.muon(1)   , eventWeight));
      if(event.muons    ()>2) ANA_CHECK(m_muon2    ->execute(event.muon(2)   , eventWeight));
    }

  //
  // Electrons
  if(!m_electronDetailStr.empty())
    {
      h_nElectron   ->Fill(event.electrons   (), eventWeight);

      if(event.electrons()>0) ANA_CHECK(m_electron0->execute(event.electron(0), eventWeight));
      if(event.electrons()>1) ANA_CHECK(m_electron1->execute(event.electron(1), eventWeight));
      if(event.electrons()>2) ANA_CHECK(m_electron2->execute(event.electron(2), eventWeight));
    }

  //
  // Fat jets
  if(!m_fatjetDetailStr.empty())
    {
      h_nFatJet->Fill(event.fatjets(), eventWeight);
      
      if(Hcand              ) ANA_CHECK(m_Hcand    ->execute(Hcand            , eventWeight));
      if(event.fatjets  ()>0) ANA_CHECK(m_fatjet0  ->execute(event.fatjet(0)  , eventWeight));
    }

  return StatusCode::SUCCESS;
}

StatusCode HbbISRHists::finalize()
{
  if(m_debug) std::cout << "HbbISRHists::finalize()" << std::endl;
  
  ANA_CHECK(EventHists::finalize());

  ANA_CHECK(m_vbf->finalize());
  delete m_vbf;

  //
  // Jets
  if(!m_jetDetailStr.empty())
    {
      ANA_CHECK(m_jet0   ->finalize());
      ANA_CHECK(m_jet1   ->finalize());
      ANA_CHECK(m_jet2   ->finalize());
      ANA_CHECK(m_jet3   ->finalize());

      delete m_jet0;
      delete m_jet1;
      delete m_jet2;
      delete m_jet3;
    }

  //
  // Muons
  if(!m_muonDetailStr.empty())
    {
      ANA_CHECK(m_muon0   ->finalize());
      ANA_CHECK(m_muon1   ->finalize());
      ANA_CHECK(m_muon2   ->finalize());

      delete m_muon0;
      delete m_muon1;
      delete m_muon2;
    }

  //
  // Electrons
  if(!m_electronDetailStr.empty())
    {
      ANA_CHECK(m_electron0   ->finalize());
      ANA_CHECK(m_electron1   ->finalize());
      ANA_CHECK(m_electron2   ->finalize());

      delete m_electron0;
      delete m_electron1;
      delete m_electron2;
    }

  //
  // Fat Jets
  if(!m_fatjetDetailStr.empty())
    {
      ANA_CHECK(m_Hcand  ->finalize());
      ANA_CHECK(m_fatjet0->finalize());

      delete m_Hcand;
      delete m_fatjet0;
    }

  return StatusCode::SUCCESS;
}
