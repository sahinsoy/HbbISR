#!/bin/bash

export PACKAGE=HbbISR
source ${AnalysisBase_PLATFORM}/bin/librun.sh

# sample list
MC_GRID="../HbbISR/filelists/Higgs_VHbb.fatjet.NTUP.list ../HbbISR/filelists/Higgs_ggFHbb.fatjet.NTUP.list ../HbbISR/filelists/Pythia8_dijet.fatjet.NTUP.list ../HbbISR/filelists/Sherpa_Wqq.fatjet.NTUP.list ../HbbISR/filelists/Sherpa_Zqq.fatjet.NTUP.list ../HbbISR/filelists/Sherpa_ttbar.fatjet.NTUP.list"

# Process
RUNLIST=""

# fatjet
runOne fatjet fatjet_mc "-m" ${MC_GRID}
#runOne higgseff higgseff "-m" ../HbbISR/filelists/Higgs_ggFHbb.fatjet.NTUP.list

for RUN in ${RUNLIST}
do
    echo "Waiting for PID ${RUN}"
    wait ${RUN} || exit 1
    echo "COMPLETED WITH ${?}"
done

#
# Merge
#

# fatjet
# mergeSys fatjet_mc ${syslist_jjj[@]}
#merge fatjet_mc hist.root "hist-*Pythia*jetjet*JZ*[0-9]W.*.root"
#merge fatjet_mc hist-ttbar.root "hist-*Sherpa_221_NNPDF30NNLO_ttbar*.root"
